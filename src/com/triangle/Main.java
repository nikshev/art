package com.triangle;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

public class Main {

    public static void main(String[] args) {
        try {
            BufferedImage srcBufferedImage = ImageIO.read(new File(img_src)); //Get image
            BufferedImage dstBufferedImage = new BufferedImage(srcBufferedImage.getWidth(), srcBufferedImage.getHeight(), srcBufferedImage.getType());

            com.jhlabs.image.OilFilter oilFilter=new com.jhlabs.image.OilFilter();
            oilFilter.setRange(2);
            oilFilter.setLevels(10);
            oilFilter.filter(srcBufferedImage, dstBufferedImage);
            File outputfile = new File(results_folder+"result_oil");
            ImageIO.write(dstBufferedImage, "jpg", outputfile);

            srcBufferedImage = ImageIO.read(new File(results_folder+"result_oil")); //Get image
            com.jhlabs.image.QuantizeFilter quantizeFilter=new com.jhlabs.image.QuantizeFilter();
            quantizeFilter.filter(srcBufferedImage, dstBufferedImage);
            quantizeFilter.setNumColors(6);
            outputfile = new File(results_folder+"result_quantize");
            ImageIO.write(dstBufferedImage, "jpg", outputfile);

            srcBufferedImage = ImageIO.read(new File(results_folder+"result_quantize")); //Get image
            com.jhlabs.image.EdgeFilter edgeFilter=new com.jhlabs.image.EdgeFilter();
            edgeFilter.filter(srcBufferedImage, dstBufferedImage);
            outputfile = new File(results_folder+"result_edge");
            ImageIO.write(dstBufferedImage, "jpg", outputfile);

            srcBufferedImage = ImageIO.read(new File(results_folder+"result_edge")); //Get image
            com.jhlabs.image.InvertFilter invertFilter= new com.jhlabs.image.InvertFilter();
            dstBufferedImage=new BufferedImage(srcBufferedImage.getWidth(), srcBufferedImage.getHeight(), srcBufferedImage.getType());
            invertFilter.filter(srcBufferedImage, dstBufferedImage);
            outputfile = new File(results_folder+"result_invert");
            ImageIO.write(dstBufferedImage, "jpg", outputfile);

            srcBufferedImage = ImageIO.read(new File(results_folder+"result_invert")); //Get image
            com.jhlabs.image.GrayscaleFilter grayscaleFilter= new com.jhlabs.image.GrayscaleFilter();
            dstBufferedImage=new BufferedImage(srcBufferedImage.getWidth(), srcBufferedImage.getHeight(), srcBufferedImage.getType());
            grayscaleFilter.filter(srcBufferedImage,dstBufferedImage);
            outputfile = new File(results_folder+"result_grayscale");
            ImageIO.write(dstBufferedImage, "jpg", outputfile);


            srcBufferedImage = ImageIO.read(new File(results_folder+"result_grayscale")); //Get image
            com.jhlabs.image.ContrastFilter contrastFilter=new com.jhlabs.image.ContrastFilter();
            dstBufferedImage=new BufferedImage(srcBufferedImage.getWidth(), srcBufferedImage.getHeight(), srcBufferedImage.getType());
            contrastFilter.setBrightness((float)0.9);
            contrastFilter.setContrast(1);
            contrastFilter.filter(srcBufferedImage,dstBufferedImage);
            outputfile = new File(results_folder+"result_contrast");
            ImageIO.write(dstBufferedImage, "jpg", outputfile);


        } catch (Exception ex){
            ex.printStackTrace();
        }

    }

    //Instance variables
    private static String img_src="/home/eugene/Desktop/experiment/src/experiment.jpg";
    private static String results_folder="/home/eugene/Desktop/experiment/results/";
}
